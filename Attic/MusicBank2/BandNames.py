# -----------------------------------------------------------------------
#
#                            < BandNames.py >
#
# -----------------------------------------------------------------------


# -----------------------------------------------------------------------
#
# File Name    : BandNames.py
#
# Author       : Josef Grosch
#
# Date         : 15 October 2015
#
# Modification : Some
#
# Application  :
#
# Description  :
#
# Notes        :
#
# Functions    :
#
# -----------------------------------------------------------------------


# -----------------------------------------------------------------------
#
#                              Copyright
#
#                   (C) Copyright 2015 Moose River LLC.
#                          < jgrosch@gmail.com >
#
#                         All Rights Reserved
#
#               Deadicated to my brother Jerry Garcia,
#            who passed from this life on August 9, 1995.
#                     Happy trails to you, Jerry
#
# -----------------------------------------------------------------------


# -----------------------------------------------------------------------
#
#                               GPG Key
#
# pub   4096R/9FDD3FC7 2012-04-29
# Key   fingerprint = 178C 7996 9292 B705 7067  7084 9EA9 459F 9FDD 3FC7
# uid   Josef Grosch <jgrosch@gmail.com>
# sub   4096R/EEA1791E 2012-04-29
#
# -----------------------------------------------------------------------


# -----------------------------------------------------------------------
#
#                         Contact Information
#
#                          Moose River LLC.
#                            P.O. Box 9403
#                         Berkeley, Ca. 94709
#
#                      http://www.mooseriver.com
#
# -----------------------------------------------------------------------


# -----------------------------------------------------------------------
#
# Import
#
# -----------------------------------------------------------------------
import sys
import os
#import urllib
#from urllib2 import Request, urlopen, URLError
#import xml.etree.ElementTree as ET
#from xml.dom import minidom


# -----------------------------------------------------------------------
#
# Class BandNames
#
# -----------------------------------------------------------------------
class BandNames:
    OddBandNames = {}
    BandNames    = {}
    BandUrl      = {}
    BandDB       = {}
    
    OddBandNames = {'jm3'      : 'John Mayer Trio',
                    'jmayer3'  : 'John Mayer Trio',
                    'kk'       : 'Kai Kln',
                    'kk'       : 'Kudzu Kings',
                    'rr'       : 'Robert Randolph & the Family Band',
                    'rr'       : 'Rusted Root',
                    'sts9'     : 'Sound Tribe Sector 9',
                    'sy'       : 'Seth Yacovone Band',
                    'sy'       : 'Sonic Youth',
                    'willyp'   : 'Willy Porter Band',
                    'willyp'   : 'Willy Porter'
                    }

    BandNames = {'3ah' : '3 Apples High',
                 'D&T' : 'Dave Matthews & Tim Reynolds',
                 'DM' : 'Dave Matthews (solo)',
                 'FB' : 'Les Claypool\'s Fearless Flying Frog Brigade',
                 'Garcia' : 'Jerry Garcia',
                 'abb' : 'Allman Brothers Band',
                 'ah&f' : 'Alan Hertz and Friends',
                 'ahf' : 'Alan Hertz and Friends',
                 'ahp' : 'Alan Hertz and Friends',
                 'amendola' : 'Scott Amendola Band',
                 'amf' : 'Amfibian',
                 'bc' : 'Black Crowes',
                 'bcue' : 'Barbara Cue',
                 'be' : 'Bockman\'s Euphio',
                 'beanland' : 'Beanlan',
                 'bfft' : 'Bela Fleck & the Flecktones',
                 'bfm' : 'Barefoot Manner',
                 'bftt' : 'Bela Fleck & Tony Trischka',
                 'bgug' : 'Blueground Undergrass',
                 'bh' : 'Ben Harper',
                 'bh' : 'Bruce Hornsby',
                 'bhic' : 'Ben Harper and the Innocent Criminals',
                 'bij' : 'Big In Japan',
                 'bmelon' : 'Blind Melon',
                 'bnb' : 'Burt Neilson Band',
                 'bog' : 'A Band of Gypsys',
                 'bp' : 'Brothers Past',
                 'bruce' : 'Bruce Hornsby',
                 'bs' : 'Big Smith',
                 'bspear' : 'Burning Spear',
                 'bt' : 'Blues Traveler',
                 'bts' : 'Built to Spill',
                 'buho' : 'El Buho',
                 'cb' : 'Critters Buggin',
                 'cbobb' : 'Col. Claypool\'s Bucket of Bernie Brains',
                 'cc' : 'Counting Crows',
                 'ccity' : 'Cerulean City',
                 'ch' : 'Charlie Hunter',
                 'cj' : 'Cowboy Junkies',
                 'claypool' : 'Les Claypool\'s Fearless Flying Frog Brigade',
                 'cracker' : 'Cracker',
                 'crowes' : 'Black Crowes',
                 'cvb' : 'Camper van Beethoven',
                 'db' : 'Disco Biscuits',
                 'dbb' : 'Deep Banana Blackout',
                 'dbr' : 'Day by the River',
                 'dbt' : 'Drive-By Truckers',
                 'ddbb' : 'Dirty Dozen Brass Band',
                 'dead' : 'The Dead',
                 'dgray' : 'David Gray',
                 'disp' : 'Dispatch',
                 'djlogic' : 'DJ Logic',
                 'dmb' : 'Dave Matthews Band',
                 'dnb' : 'David Nelson Band',
                 'dso' : 'Dark Star Orchestra',
                 'dt' : 'Derek Trucks Band',
                 'dtb' : 'Donna the Buffalo',
                 'eb' : 'Edie Brickell',
                 'eh' : 'Ekoostik Hookah',
                 'farrah' : 'Farrah',
                 'fffb' : 'Les Claypool\'s Fearless Flying Frog Brigade',
                 'fhg' : 'Fareed Haque Group',
                 'gal' : 'Galactic',
                 'garaj' : 'Garaj Mahal',
                 'gba' : 'GreyBoy AllStars',
                 'gd' : 'Grateful Dead',
                 'glove' : 'G. Love and Special Sauce',
                 'gm' : 'Gov\'t Mule',
                 'gsw' : 'God Street Wine',
                 'gt' : 'Ghost Trane',
                 'gtb' : 'Grand Theft Bus',
                 'gus' : 'Guster',
                 'guster' : 'Guster',
                 'guymalone' : 'Guy Malone',
                 'hday' : 'Howie Day',
                 'hip' : 'The Tragically Hip',
                 'ho' : 'Schleigho',
                 'ht' : 'Hot Tuna',
                 'ig' : 'Indigo Girls',
                 'itf' : 'Indiana Trip Factory',
                 'jg' : 'Jerry Garcia',
                 'jgb' : 'Jerry Garcia Band',
                 'jh' : 'Jimi Hendrix Experience',
                 'jhbg' : 'A Band of Gypsys',
                 'jhe' : 'Jimi Hendrix Experience',
                 'jj' : 'Jack Johnson',
                 'jjj' : 'Jerry Joseph & The Jackmormons',
                 'jk' : 'Jorma Kaukonen',
                 'jmayer' : 'John Mayer',
                 'jmos' : 'Jerry Joseph & The Jackmormons',
                 'jmp' : 'Jazz Mandolin Project',
                 'jol' : 'Jolene',
                 'jt' : 'Jeff Tweedy',
                 'kaikln' : 'Kai Kln',
                 'kdcw' : 'Karl Denson and Chris Wood',
                 'kdtu' : 'Karl Denson\'s Tiny Universe',
                 'kdub' : 'Keller Williams',
                 'kmck' : 'Steve Kimock & Friends',
                 'kudzu' : 'Kudzu Kings',
                 'kvhw' : 'KVHW',
                 'kw' : 'Keller Williams',
                 'kwi' : 'Keller Williams String Cheese Incident',
                 'laf' : 'Life After Failing',
                 'lcbbb' : 'Col. Claypool\'s Bucket of Bernie Brains',
                 'lcbobb' : 'Col. Claypool\'s Bucket of Bernie Brains',
                 'lcfffb' : 'Les Claypool\'s Fearless Flying Frog Brigade',
                 'ld' : 'Living Daylights',
                 'lf' : 'Little Feat',
                 'lfb' : 'Lo Faber Band',
                 'logic' : 'DJ Logic',
                 'lom' : 'Legion of Mary',
                 'los' : 'Leftover Salmon',
                 'ls' : 'Leftover Salmon',
                 'lt' : 'Lake Trout',
                 'mammals' : 'The Mammals',
                 'marlow' : 'Marlow',
                 'mcrx' : 'Mike Clark\'s Prescription Renewal',
                 'mel' : 'Marcus Eaton & The Lobby',
                 'metheny' : 'Pat Metheny',
                 'mf' : 'Marc Ford',
                 'mfs' : 'Michael Franti & Spearhead',
                 'mmw' : 'Medeski Martin & Wood',
                 'moe' : 'moe.',
                 'mule' : 'Gov\'t Mule',
                 'nd' : 'The New Deal',
                 'nmas' : 'North Mississippi Allstars',
                 'oar' : 'O.A.R. (Of A Revolution)',
                 'oh' : 'Oysterhead',
                 'or' : 'Oregon',
                 'oregon' : 'Oregon',
                 'oysterhead' : 'Oysterhead',
                 'p&f' : 'Phil Lesh & Friends',
                 'p+f' : 'Phil Lesh & Friends',
                 'paf' : 'Phil Lesh & Friends',
                 'par' : 'Particle',
                 'particle' : 'Particle',
                 'pb' : 'Psychedelic Breakfast',
                 'pf' : 'Phil Lesh & Friends',
                 'ph' : 'Phish',
                 'phil' : 'Phil Lesh & Friends',
                 'pj' : 'Pearl Jam',
                 'pm' : 'Pat Metheny',
                 'pmb' : 'Pat McGee Band',
                 'pmg' : 'Pat Metheny Group',
                 'pmt' : 'Pat Metheny Trio',
                 'pnf' : 'Phil Lesh & Friends',
                 'porter' : 'George Porter, Jr. & Runnin\' Pardners',
                 'rad' : 'Radiators',
                 'rads' : 'Radiators',
                 'raq' : 'Raq',
                 'ratdog' : 'Ratdog',
                 'rd' : 'Ratdog',
                 'reservoir' : 'Reservoir',
                 'rezi' : 'Rezi',
                 'rg' : 'Reid Genauer',
                 'rre' : 'Railroad Earth',
                 'rrfb' : 'Robert Randolph & the Family Band',
                 'rw2c' : 'Robert Walter\'s 20th Congress',
                 'rwtc' : 'Robert Walter\'s 20th Congress',
                 'schas' : 'Santa Cruz Hemp Allstars',
                 'schwillb' : 'The Schwillbillies',
                 'sci' : 'String Cheese Incident',
                 'sco' : 'John Scofield',
                 'sexmob' : 'Sex Mob',
                 'sf' : 'Strangefolk',
                 'sk' : 'Kimock',
                 'skb' : 'Steve Kimock Band',
                 'sl' : 'The Slip',
                 'slip' : 'The Slip',
                 'sonicyouth' : 'Sonic Youth',
                 'soulive' : 'Soulive',
                 'spin' : 'Spin Doctors',
                 'spod' : 'Serial Pod',
                 'spr' : 'Michael Franti & Spearhead',
                 'ss' : 'Stockholm Syndrome',
                 'st' : 'Shaking Tree',
                 'stocksyn' : 'Stockholm Syndrome',
                 'syb' : 'Seth Yacovone Band',
                 'tab' : 'Trey Anastasio (Band)',
                 'td' : 'The Dead',
                 'tend' : 'Tenacious D',
                 'tlg' : 'Tea Leaf Green',
                 'tn' : 'The Nadas',
                 'tnd' : 'The New Deal',
                 'too' : 'The Other Ones',
                 'tortoise' : 'Tortoise',
                 'tr' : 'Tim Reynolds',
                 'trey' : 'Trey Anastasio (Band)',
                 'um' : 'Umphrey\'s McGee',
                 'umph' : 'Umphrey\'s McGee',
                 'us' : 'Uncle Sammy',
                 'vb' : 'Vida Blue',
                 'wb4t' : 'Will Bernard 4tet',
                 'ween' : 'Ween',
                 'wh' : 'Warren Haynes',
                 'wilco' : 'Wilco',
                 'word' : 'The Word',
                 'wp' : 'Widespread Panic',
                 'wsp' : 'Widespread Panic',
                 'wu' : 'The Big Wu',
                 'ymsb' : 'Yonder Mountain String Band',
                 'zero' : 'Zero',
                 'zm' : 'Zony Mash',
                 'zwan' : 'Zwan'
                 }
    

    BandUrl = {'3ah' : 'www.myspace.com/3appleshighmusic', # '3 Apples High',
               'D&T' : 'www.davematthewsband.com', # 'Dave Matthews & Tim Reynolds',
               'DM' : 'www.davematthewsband.com', # 'Dave Matthews (solo)',
               'FB' : 'www.lesclaypool.com', # 'Les Claypool\'s Fearless Flying Frog Brigade',
               'Garcia' : 'www.dead.net', # 'Jerry Garcia',
               'abb' : 'www.allmanbrothersband.com', # 'Allman Brothers Band',
               'ah&f' : 'www.garajmahaljams.com', # 'Alan Hertz and Friends',
               'ahf' : 'www.garajmahaljams.com', # 'Alan Hertz and Friends',
               'ahp' : 'www.garajmahaljams.com', # 'Alan Hertz and Friends',
               'amendola' : 'www.scottamendola.com', # 'Scott Amendola Band',
               'amf' : 'www.myspace.com/Amfibian', # 'Amfibian',
               'bc' : 'www.blackcrowes.com', # 'Black Crowes',
               'bcue' : 'www.myspace.com/bbcue', # 'Barbara Cue',
               'be' : 'www.euphio.net', # 'Bockman\'s Euphio',
               'beanland' : 'www.beanland.net', # 'Beanlan',
               'bfft' : 'www.belafleck.com', # 'Bela Fleck & the Flecktones',
               'bfm' : 'www.myspace.com/barefootmanner', # 'Barefoot Manner',
               'bftt' : 'www.belafleck.com', # 'Bela Fleck & Tony Trischka',
               'bgug' : 'www.bluegroundundergrass.com', # 'Blueground Undergrass',
               'bh' : 'www.benharper.com', # 'Ben Harper',
               'bh' : 'www.brucehornsby.com', # 'Bruce Hornsby',
               'bhic' : 'www.benharper.com', # 'Ben Harper and the Innocent Criminals',
               'bij' : 'www.myspace.com/bigjapan', # 'Big In Japan',
               'bmelon' : 'www.myspace.com/blindmelon', # 'Blind Melon',
               'bnb' : 'www.myspace.com/burtneilsonband', # 'Burt Neilson Band',
               'bog' : 'UNKNOWN', # 'A Band of Gypsys',
               'bp' : 'www.brotherspast.com', # 'Brothers Past',
               'bruce' : 'www.brucehornsby.com', # 'Bruce Hornsby',
               'bs' : 'www.bigsmithband.com', # 'Big Smith',
               'bspear' : 'www.burningspear.net', # 'Burning Spear',
               'bt' : 'www.bluestraveler.com', # 'Blues Traveler',
               'bts' : 'www.builttospill.com', # 'Built to Spill',
               'buho' : 'www.elbuho.com', # 'El Buho',
               'cb' : 'www.crittersbuggin.com', # 'Critters Buggin',
               'cbobb' : 'www.lesclaypool.com', # 'Col. Claypool\'s Bucket of Bernie Brains',
               'cc' : 'www.countingcrows.com', # 'Counting Crows',
               'ccity' : 'www.myspace.com/ceruleancity', # 'Cerulean City',
               'ch' : 'www.charliehunter.com', # 'Charlie Hunter',
               'cj' : 'latentrecordings.com/cowboyjunkies', # 'Cowboy Junkies',
               'claypool' : 'www.lesclaypool.com', # 'Les Claypool\'s Fearless Flying Frog Brigade',
               'cracker' : 'www.crackersoul.com', # 'Cracker',
               'crowes' : 'www.blackcrowes.com', # 'Black Crowes',
               'cvb' : 'www.campervanbeethoven.com', # 'Camper van Beethoven',
               'db' : 'www.discobiscuits.com', # 'Disco Biscuits',
               'dbb' : 'www.deepbananablackout.com', # 'Deep Banana Blackout',
               'dbr' : 'www.daybytheriver.org', # 'Day by the River',
               'dbt' : 'www.drivebytruckers.com', # 'Drive-By Truckers',
               'ddbb' : 'www.dirtydozenbrass.com', # 'Dirty Dozen Brass Band',
               'dead' : 'www.dead.net', # 'The Dead',
               'dgray' : 'www.davidgray.com', # 'David Gray',
               'disp' : 'www.dispatchmusic.com', # 'Dispatch',
               'djlogic' : 'www.djlogic.com', # 'DJ Logic',
               'dmb' : 'www.davematthewsband.com', # 'Dave Matthews Band',
               'dnb' : 'www.nelsonband.com', # 'David Nelson Band',
               'dso' : 'www.darkstarorchestra.net', # 'Dark Star Orchestra',
               'dt' : 'www.derektrucks.com', # 'Derek Trucks Band',
               'dtb' : 'www.donnathebuffalo.com', # 'Donna the Buffalo',
               'eb' : 'www.ediebrickell.com', # 'Edie Brickell',
               'eh' : 'www.ekoostik.com', # 'Ekoostik Hookah',
               'farrah' : 'UNKNOWN', # 'Farrah',
               'fffb' : 'www.lesclaypool.com', # 'Les Claypool\'s Fearless Flying Frog Brigade',
               'fhg' : 'UNKNOWN', # 'Fareed Haque Group',
               'gal' : 'UNKNOWN', # 'Galactic',
               'garaj' : 'www.garajmahaljams.com', # 'Garaj Mahal',
               'gba' : 'UNKNOWN', # 'GreyBoy AllStars',
               'gd' : 'www.dead.net', # 'Grateful Dead',
               'glove' : 'UNKNOWN', # 'G. Love and Special Sauce',
               'gm' : 'www.mule.net', # 'Gov\'t Mule',
               'gsw' : 'UNKNOWN', # 'God Street Wine',
               'gt' : 'UNKNOWN', # 'Ghost Trane',
               'gtb' : 'UNKNOWN', # 'Grand Theft Bus',
               'gus' : 'UNKNOWN', # 'Guster',
               'guster' : 'UNKNOWN', # 'Guster',
               'guymalone' : 'UNKNOWN', # 'Guy Malone',
               'hday' : 'UNKNOWN', # 'Howie Day',
               'hip' : 'www.thehip.com', # 'The Tragically Hip',
               'ho' : 'UNKNOWN', # 'Schleigho',
               'ht' : 'www.hottuna.com', # 'Hot Tuna',
               'ig' : 'UNKNOWN', # 'Indigo Girls',
               'itf' : 'UNKNOWN', # 'Indiana Trip Factory',
               'jg' : 'www.dead.net', # 'Jerry Garcia',
               'jgb' : 'www.dead.net', # 'Jerry Garcia Band',
               'jh' : 'www.jimi-hendrix.com', # 'Jimi Hendrix Experience',
               'jhbg' : 'UNKNOWN', # 'A Band of Gypsys',
               'jhe' : 'www.jimi-hendrix.com', # 'Jimi Hendrix Experience',
               'jj' : 'UNKNOWN', # 'Jack Johnson',
               'jjj' : 'UNKNOWN', # 'Jerry Joseph & The Jackmormons',
               'jk' : 'www.hottuna.com', # 'Jorma Kaukonen',
               'jmayer' : 'UNKNOWN', # 'John Mayer',
               'jmos' : 'UNKNOWN', # 'Jerry Joseph & The Jackmormons',
               'jmp' : 'UNKNOWN', # 'Jazz Mandolin Project',
               'jol' : 'UNKNOWN', # 'Jolene',
               'jt' : 'www.wilcoworld.net', # 'Jeff Tweedy',
               'kaikln' : 'UNKNOWN', # 'Kai Kln',
               'kdcw' : 'UNKNOWN', # 'Karl Denson and Chris Wood',
               'kdtu' : 'UNKNOWN', # 'Karl Denson\'s Tiny Universe',
               'kdub' : 'UNKNOWN', # 'Keller Williams',
               'kmck' : 'UNKNOWN', # 'Steve Kimock & Friends',
               'kudzu' : 'UNKNOWN', # 'Kudzu Kings',
               'kvhw' : 'UNKNOWN', # 'KVHW',
               'kw' : 'UNKNOWN', # 'Keller Williams',
               'kwi' : 'UNKNOWN', # 'Keller Williams String Cheese Incident',
               'laf' : 'UNKNOWN', # 'Life After Failing',
               'lcbbb' : 'www.lesclaypool.com', # 'Col. Claypool\'s Bucket of Bernie Brains',
               'lcbobb' : 'www.lesclaypool.com', # 'Col. Claypool\'s Bucket of Bernie Brains',
               'lcfffb' : 'www.lesclaypool.com', # 'Les Claypool\'s Fearless Flying Frog Brigade',
               'ld' : 'UNKNOWN', # 'Living Daylights',
               'lf' : 'UNKNOWN', # 'Little Feat',
               'lfb' : 'UNKNOWN', # 'Lo Faber Band',
               'logic' : 'UNKNOWN', # 'DJ Logic',
               'lom' : 'www.dead.net', # 'Legion of Mary',
               'los' : 'www.leftoversalmon.com', # 'Leftover Salmon',
               'ls' : 'www.leftoversalmon.com', # 'Leftover Salmon',
               'lt' : 'UNKNOWN', # 'Lake Trout',
               'mammals' : 'UNKNOWN', # 'The Mammals',
               'marlow' : 'UNKNOWN', # 'Marlow',
               'mcrx' : 'UNKNOWN', # 'Mike Clark\'s Prescription Renewal',
               'mel' : 'UNKNOWN', # 'Marcus Eaton & The Lobby',
               'metheny' : 'UNKNOWN', # 'Pat Metheny',
               'mf' : 'UNKNOWN', # 'Marc Ford',
               'mfs' : 'UNKNOWN', # 'Michael Franti & Spearhead',
               'mmw' : 'UNKNOWN', # 'Medeski Martin & Wood',
               'moe' : 'www.moe.org', # 'moe.',
               'mule' : 'www.mule.net', # 'Gov\'t Mule',
               'nd' : 'UNKNOWN', # 'The New Deal',
               'nmas' : 'UNKNOWN', # 'North Mississippi Allstars',
               'oar' : 'UNKNOWN', # 'O.A.R. (Of A Revolution)',
               'oh' : 'www.oysterhead.com', # 'Oysterhead',
               'or' : 'www.oregonband.com', # 'Oregon',
               'oregon' : 'www.oregonband.com', # 'Oregon',
               'oysterhead' : 'www.oysterhead.com', # 'Oysterhead',
               'p&f' : 'www.phillesh.net', # 'Phil Lesh & Friends',
               'p+f' : 'www.phillesh.net', # 'Phil Lesh & Friends',
               'paf' : 'www.phillesh.net', # 'Phil Lesh & Friends',
               'par' : 'www.particlepeople.com', # 'Particle',
               'particle' : 'www.particlepeople.com', # 'Particle',
               'pb' : 'www.thebreakfast.org', # 'Psychedelic Breakfast',
               'pf' : 'www.phillesh.net', # 'Phil Lesh & Friends',
               'ph' : 'www.phish.com', # 'Phish',
               'phil' : 'www.phillesh.net', # 'Phil Lesh & Friends',
               'pj' : 'UNKNOWN', # 'Pearl Jam',
               'pm' : 'UNKNOWN', # 'Pat Metheny',
               'pmb' : 'UNKNOWN', # 'Pat McGee Band',
               'pmg' : 'UNKNOWN', # 'Pat Metheny Group',
               'pmt' : 'UNKNOWN', # 'Pat Metheny Trio',
               'pnf' : 'www.phillesh.net', # 'Phil Lesh & Friends',
               'porter' : 'UNKNOWN', # 'George Porter, Jr. & Runnin\' Pardners',
               'rad' : 'www.theradiators.org', # 'Radiators',
               'rads' : 'www.theradiators.org', # 'Radiators',
               'raq' : 'UNKNOWN', # 'Raq',
               'ratdog' : 'www.rat-dog.com', # 'Ratdog',
               'rd' : 'www.rat-dog.com', # 'Ratdog',
               'reservoir' : 'UNKNOWN', # 'Reservoir',
               'rezi' : 'UNKNOWN', # 'Rezi',
               'rg' : 'UNKNOWN', # 'Reid Genauer',
               'rre' : 'UNKNOWN', # 'Railroad Earth',
               'rrfb' : 'UNKNOWN', # 'Robert Randolph & the Family Band',
               'rw2c' : 'UNKNOWN', # 'Robert Walter\'s 20th Congress',
               'rwtc' : 'UNKNOWN', # 'Robert Walter\'s 20th Congress',
               'schas' : 'UNKNOWN', # 'Santa Cruz Hemp Allstars',
               'schwillb' : 'UNKNOWN', # 'The Schwillbillies',
               'sci' : 'UNKNOWN', # 'String Cheese Incident',
               'sco' : 'UNKNOWN', # 'John Scofield',
               'sexmob' : 'UNKNOWN', # 'Sex Mob',
               'sf' : 'UNKNOWN', # 'Strangefolk',
               'sk' : 'www.kimock.com', # 'Kimock',
               'skb' : 'www.kimock.com', # 'Steve Kimock Band',
               'sl' : 'UNKNOWN', # 'The Slip',
               'slip' : 'UNKNOWN', # 'The Slip',
               'sonicyouth' : 'www.sonicyouth.com', # 'Sonic Youth',
               'soulive' : 'UNKNOWN', # 'Soulive',
               'spin' : 'UNKNOWN', # 'Spin Doctors',
               'spod' : 'UNKNOWN', # 'Serial Pod',
               'spr' : 'UNKNOWN', # 'Michael Franti & Spearhead',
               'ss' : 'UNKNOWN', # 'Stockholm Syndrome',
               'st' : 'UNKNOWN', # 'Shaking Tree',
               'stocksyn' : 'UNKNOWN', # 'Stockholm Syndrome',
               'syb' : 'UNKNOWN', # 'Seth Yacovone Band',
               'tab' : 'UNKNOWN', # 'Trey Anastasio (Band)',
               'td' : 'www.dead.net', # 'The Dead',
               'tend' : 'UNKNOWN', # 'Tenacious D',
               'tlg' : 'UNKNOWN', # 'Tea Leaf Green',
               'tn' : 'UNKNOWN', # 'The Nadas',
               'tnd' : 'UNKNOWN', # 'The New Deal',
               'too' : 'www.dead.net', # 'The Other Ones',
               'tortoise' : 'UNKNOWN', # 'Tortoise',
               'tr' : 'UNKNOWN', # 'Tim Reynolds',
               'trey' : 'UNKNOWN', # 'Trey Anastasio (Band)',
               'um' : 'www.umphreys.com', # 'Umphrey\'s McGee',
               'umph' : 'www.umphreys.com', # 'Umphrey\'s McGee',
               'us' : 'UNKNOWN', # 'Uncle Sammy',
               'vb' : 'UNKNOWN', # 'Vida Blue',
               'wb4t' : 'UNKNOWN', # 'Will Bernard 4tet',
               'ween' : 'UNKNOWN', # 'Ween',
               'wh' : 'UNKNOWN', # 'Warren Haynes',
               'wilco' : 'www.wilcoworld.net', # 'Wilco',
               'word' : 'UNKNOWN', # 'The Word',
               'wp' : 'www.widespreadpanic.com', # 'Widespread Panic',
               'wsp' : 'www.widespreadpanic.com', # 'Widespread Panic',
               'wu' : 'UNKNOWN', # 'The Big Wu',
               'ymsb' : 'UNKNOWN', # 'Yonder Mountain String Band',
               'zero' : 'www.zerolive.com', # 'Zero',
               'zm' : 'UNKNOWN', # 'Zony Mash',
               'zwan' : 'UNKNOWN' # 'Zwan'
               }


    # -----------------------------------------------------------------------
    #
    # __init__
    #
    # -----------------------------------------------------------------------
    def __init__(self):
        """
        Initializes this class with the following variables

        debug = False
        """

        self.debug = False

        #
        # End of __init__
        #

    # -----------------------------------------------------------------------
    #
    # turnOnDebug
    #
    # -----------------------------------------------------------------------
    def turnOnDebug(self):
        """
        Set the debug flag to True
        """

        self.debug = True

        return
        #
        # End of turnOnDebug
        #

        
    # -----------------------------------------------------------------------
    #
    # turnOffDebug
    #
    # -----------------------------------------------------------------------
    def turnOffDebug(self):
        """
        Set the debug flag to False
        """

        self.debug = False

        return
        #
        # End of turnOffDebug
        #

    # -----------------------------------------------------------------------
    #
    # loadBandDB
    #
    # -----------------------------------------------------------------------
    def loadBandDB(self):
        # D = {}
        # D['a'] = {'name': 'bob', 'age': 43}
        # name = D['a']['name']

        for name in BandNames:
            full_name = BandNames[name]
            url = BandUrl[name]
            BandDB[name] = {'full_name': full_name, 'url': url}

        return
        #
        # End of loadBandDB
        #

    def createBandDB(self, D):
        fqp = D['fqp']
        dbh = D['dbh']
        
        return
        #
        # End of createBandDB
        #


    #
    # End of class BandNames
    #

# -----------------------------------------------------------------------
#
#                          < End of BandNames >
#
# -----------------------------------------------------------------------
