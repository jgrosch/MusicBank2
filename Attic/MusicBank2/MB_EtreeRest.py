# -----------------------------------------------------------------------
#
#                           < MB_EtreeRest.py >
#
# -----------------------------------------------------------------------


# -----------------------------------------------------------------------
#
# File Name    : MB_EtreeRest.py
#
# Author       : Josef Grosch
#
# Date         : 18 June 2015
#
# Modification : Some
#
# Application  :
#
# Description  :
#
# Notes        :
#
# Functions    :
#
# -----------------------------------------------------------------------


# -----------------------------------------------------------------------
#
#                              Copyright
#
#                   (C) Copyright 2015 Moose River LLC.
#                          < jgrosch@gmail.com >
#
#                         All Rights Reserved
#
#               Deadicated to my brother Jerry Garcia,
#            who passed from this life on August 9, 1995.
#                     Happy trails to you, Jerry
#
# -----------------------------------------------------------------------


# -----------------------------------------------------------------------
#
#                               GPG Key
#
# pub   4096R/9FDD3FC7 2012-04-29
# Key   fingerprint = 178C 7996 9292 B705 7067  7084 9EA9 459F 9FDD 3FC7
# uid   Josef Grosch <jgrosch@gmail.com>
# sub   4096R/EEA1791E 2012-04-29
#
# -----------------------------------------------------------------------


# -----------------------------------------------------------------------
#
#                         Contact Information
#
#                          Moose River LLC.
#                            P.O. Box 9403
#                         Berkeley, Ca. 94709
#
#                      http://www.mooseriver.com
#
# -----------------------------------------------------------------------


# -----------------------------------------------------------------------
#
# Import
#
# -----------------------------------------------------------------------
import urllib
import sys
import os
from urllib2 import Request, urlopen, URLError
import xml.etree.ElementTree as ET
from xml.dom import minidom


# -----------------------------------------------------------------------
#
# Class MB_EtreeRest
#
# -----------------------------------------------------------------------
class MB_EtreeRest:


    # -----------------------------------------------------------------------
    #
    # getBaseURL
    #
    # -----------------------------------------------------------------------
    def getBaseURL(self):
        """
        Returns the base URL
        """
        
        return self.base_url
        #
        # End of getBaseURL
        #

        
    # -----------------------------------------------------------------------
    #
    # setBaseURL
    #
    # -----------------------------------------------------------------------
    def setBaseURL(self, url):
        """
        Sets the base URL
        """
        
        self.base_url = url
        
        return
        #
        # End of setBaseURL
        #


    # -----------------------------------------------------------------------
    #
    # setServiceKey
    #
    # -----------------------------------------------------------------------
    def setServiceKey(self, service_key):
        """
        Sets the service key i.e the license key to the etree database
        """

        self.service_key = service_key
        
        return
        #
        # End of setServiceKey
        #

    
    # -----------------------------------------------------------------------
    #
    # getServiceKey
    #
    # -----------------------------------------------------------------------
    def getServiceKey(self):
        """
        Returns the service key i.e the license key to the etree database
        """
        
        return self.service_key
        #
        # End of getServiceKey
        #

        
    # -----------------------------------------------------------------------
    #
    # turnOnDebug
    #
    # -----------------------------------------------------------------------
    def turnOnDebug(self):
        """
        Set the debug flag to True
        """

        self.debug = True

        return
        #
        # End of turnOnDebug
        #

        
    # -----------------------------------------------------------------------
    #
    # turnOffDebug
    #
    # -----------------------------------------------------------------------
    def turnOffDebug(self):
        """
        Set the debug flag to False
        """

        self.debug = False

        return
        #
        # End of turnOffDebug
        #


    # -----------------------------------------------------------------------
    #
    # __requestAnswer
    #
    # -----------------------------------------------------------------------
    def __requestAnswer(self, p_array):
        """
        This method builds the REST query and make the call returning the
        results of the call.
        """

        if len(self.service_key) <= 0:
            service_key = p_array.get('service_key')
            if service_key == None:
                print "Service key not found\n"
                sys.exit(0)
            else:
                pass
                #service_key = p_array['service_key']
        else:
            service_key = self.service_key
            
        method      = p_array['method']
        debug       = self.debug
        artist_key  = ""
        name        = ""

        #
        # createMd5
        #
        if method == 'createMd5':
            source_key = p_array['source_key']
            title      = urllib.urlencode(p_array['title'])
            text       = urllib.urlencode(p_array['text'])
            url_str = "method=%s&service_key=%s&source_key=%s&title=%s&text=%s" % \
                      (method, service_key, source_key, title, text)
        #
        # deleteMd5
        #
        elif method == 'deleteMd5':
            md5_key = p_array['md5_key']
            url_str = "method=%s&service_key=%s&md5_key=%s" % \
                      (method, service_key, md5_key)
        #
        # fetchArtist
        #
        elif method == 'fetchArtist':
            artist_key  = p_array['artist_key']
            url_str = "method=%s&service_key=%s&artist_key=%s" % \
                      (method, service_key, artist_key)
        #
        # fetchArtistYear
        #
        elif method == 'fetchArtistYear':
            artist_key = p_array['artist_key']
            year       = p_array['year']
            url_str = "method=%s&service_key=%s&artist_key=%s&year=%s" % \
                      (method, service_key, artist_key, year)
        #
        # fetchChangedArtists
        #
        elif method == 'fetchChangedArtists':
            year  = p_array['year']
            month = p_array['month']
            day   = p_array['day']
            url_str = "method=%s&service_key=%s&year=%s&month=%s&day=%s" % \
                      (method, service_key, year, month, day)
        #
        # fetchChangedShows
        #
        elif method == 'fetchChangedShows':
            artist_key = p_array['artist_key']
            year       = p_array['year']
            month      = p_array['month']
            day        = p_array['day']
            url_str = "method=%s&service_key=%s&artist_key=%s&year=%s&month=%s&day=%s" % \
                      (method, service_key, artist_key, year, month, day)
        #
        # fetchChangedSources
        #
        elif method == 'fetchChangedSources':
            artist_key = p_array['artist_key']
            year       = p_array['year']
            month      = p_array['month']
            day        = p_array['day']
            url_str = "method=%s&service_key=%s&artist_key=%s&year=%s&month=%s&day=%s" % \
                      (method, service_key, artist_key, year, month, day)
        #
        # fetchRecentSources
        #
        elif method == 'fetchRecentSources':
            artist_key = p_array['artist_key']
            year       = p_array['year']
            month      = p_array['month']
            day        = p_array['day']
            url_str = "method=%s&service_key=%s&artist_key=%s&year=%s&month=%s&day=%s" % \
                      (method, service_key, artist_key, year, month, day)
        #
        # fetchShow
        #
        elif method == 'fetchShow':
            shows_key = p_array['shows_key']
            url_str = "method=%s&service_key=%s&shows_key=%s" % \
                      (method, service_key, shows_key)
        #
        # fetchShowSources
        #
        elif method == 'fetchShowSources':
            shows_key = p_array['shows_key']
            url_str = "method=%s&service_key=%s&shows_key=%s" % \
                      (method, service_key, shows_key)
        #
        # fetchSource
        #
        elif method == 'fetchSource':
            source_key = p_array['source_key']
            url_str = "method=%s&service_key=%s&source_key=%s" % \
                      (method, service_key, source_key)
        #
        # fetchSourceYear
        #
        elif method == 'fetchSourceYear':
            artist_key = p_array['artist_key']
            year       = p_array['year']
            url_str = "method=%s&service_key=%s&artist_key=%s&year=%s" % \
                      (method, service_key, artist_key, year)
        #
        # lookupArtist
        #
        elif method == 'lookupArtist':
            name = p_array['name']
            name = urllib.quote(name)
            url_str = "method=%s&service_key=%s&name=%s" % \
                      (method, service_key, name)
            #url_str = urllib.quote(url_str)
        #
        # lookupMd5
        #
        elif method == 'lookupMd5':
            md5 = p_array['md5']
            url_str = "method=%s&service_key=%s&md5=%s" % \
                      (method, service_key, md5)
        #
        # lookupShow
        #
        elif method == 'lookupShow':
            artist_key = p_array['artist_key']
            year       = p_array['year']
            month      = p_array['month']
            day        = p_array['day']
            url_str = "method=%s&service_key=%s&artist_key=%s&year=%s&month=%s&day=%s" % \
                      (method, service_key, artist_key, year, month, day)

        #
        # fetchShowTorrents
        #
        elif method == 'fetchShowTorrents':
            burl = 'http://db.etree.org/rest/bt.php'
            self.setBaseURL(burl)
            shows_key = p_array['shows_key']
            url_str = "method=%s&service_key=%s&shows_key=%s" % \
                      (method, service_key, shows_key)
            
        #
        # fetchRecentTorrents
        #
        elif method == 'fetchRecentTorrents':
            burl = 'http://db.etree.org/rest/bt.php'
            self.setBaseURL(burl)
            artist_key = p_array['artist_key']
            year       = p_array['year']
            month      = p_array['month']
            day        = p_array['day']
            limit      = p_array['limit']
            if limit > 50:
                limit = 50
            url_str = "method=%s&service_key=%s&artist_key=%s&year=%s&month=%s&day=%s&limit=%s" % \
                      (method, service_key, artist_key, year, month, day, limit)
            

        url = "%s?%s" % (self.base_url, url_str)
        if debug:
            print "%s\n" % (url)
        
        request = Request(url)
        
        try:
            response = urlopen(request)
            data = response.read()
        except URLError, e:
            print 'No data returned', e
        
        return data
    #
    # End of __requestAnswer
    #
    

    # -----------------------------------------------------------------------
    #
    # __init__
    #
    # -----------------------------------------------------------------------
    def __init__(self):
        """
        Initializes this class with the following variables

        base_url    = http://db.etree.org/rest/db.php
        debug       = False
        service_key = ''
        """

        self.data        = {}
        self.debug       = False
        self.service_key = ''
        self.base_url    = 'http://db.etree.org/rest/db.php'

        #
        # End of __init__
        #


    # -----------------------------------------------------------------------
    #
    # fetchArtist
    #
    # -----------------------------------------------------------------------
    def fetchArtist(self, p_array):
        """
        Returns an array with the artist key and name for the given artist.
        Values passed in: service key, artist key
        """

        p_array['method'] = 'fetchArtist'
        
        o_str = self.__requestAnswer(p_array)
        
        return o_str
        #
        # End of fetchArtist
        #


    # -----------------------------------------------------------------------
    #
    # fetchShow
    #
    # -----------------------------------------------------------------------
    def fetchShow(self, p_array):
        """
        Returns a single show entry. You must provide the shows_key,
        aka showid, for the exact show you want.
        Values passed in: service key, show key
        """
        
        p_array['method'] = 'fetchShow'

        o_str = self.__requestAnswer(p_array)
        
        return o_str
        #
        # End of fetchShow
        #


    # -----------------------------------------------------------------------
    #
    # lookupShow
    #
    # -----------------------------------------------------------------------
    def lookupShow(self, p_array):
        """
        Returns an array of shows matching the artist_key, year, month,
        and day. Some artists have more than one show per day. This method
        will always return an array of shows. A maximum of 25 shows will
        be returned.
        Values passed in: service key, artist key, year, month, day
        """
        
        p_array['method'] = 'lookupShow'

        o_str = self.__requestAnswer(p_array)
        
        return o_str
        #
        # End of lookupShow
        #


    # -----------------------------------------------------------------------
    #
    # lookupArtist
    #
    # -----------------------------------------------------------------------
    def lookupArtist(self, p_array):
        """
        Returns an array of matching artist names. You may include %
        wildcards. This method will return a maximum of 25 artist names.
        Values passed in: service key, show key
        """
        
        p_array['method'] = 'lookupArtist'

        o_str = self.__requestAnswer(p_array)
        
        return o_str
        #
        # End of lookupArtist
        #


    # -----------------------------------------------------------------------
    #
    # lookupMd5
    #
    # -----------------------------------------------------------------------
    def lookupMd5(self, p_array):
        """
        Returns an array of source keys with a matching md5. Your md5 must
        be 32 characters long exactly. Will always return an array of source
        keys even though most queries will only have one result.
        Values passed in: service key, show key
        """
        
        p_array['method'] = 'lookupMd5'

        o_str = self.__requestAnswer(p_array)
        
        return o_str
        #
        # End of lookupMd5
        #


    # -----------------------------------------------------------------------
    #
    # fetchArtistYear
    #
    # -----------------------------------------------------------------------
    def fetchArtistYear(self, p_array):
        """
        Returns an array of show information for all shows by the selected
        artist in the given year.
        Values passed in: service key, show key
        """
        
        p_array['method'] = 'fetchArtistYear'

        o_str = self.__requestAnswer(p_array)
        
        return o_str
        #
        # End of fetchArtistYear
        #


    # -----------------------------------------------------------------------
    #
    # fetchSource
    #
    # -----------------------------------------------------------------------
    def fetchSource(self, p_array):
        """
        Returns a single source record for the given source_key, aka shnid.
        Values passed in: service key, show key
        """
    
        p_array['method'] = 'fetchSource'

        o_str = self.__requestAnswer(p_array)
        
        return o_str
        #
        # End of XXX
        #


    # -----------------------------------------------------------------------
    #
    # fetchSourceYear
    #
    # -----------------------------------------------------------------------
    def fetchSourceYear(self, p_array):
        """
        Returns an array of sources for the selected artist for the given year.
        Values passed in: service key, show key
        """
        
        p_array['method'] = 'fetchSourceYear'

        o_str = self.__requestAnswer(p_array)
        
        return o_str
        #
        # End of XXX
        #


    # -----------------------------------------------------------------------
    #
    # fetchChangedShows
    #
    # -----------------------------------------------------------------------
    def fetchChangedShows(self, p_array):
        """
        Returns an array of shows_keys which have been changed since the given
        date. Use artist_key=0 to fetch all shows.
        Values passed in: service key, show key
        """
        
        p_array['method'] = 'fetchChangedShows'

        o_str = self.__requestAnswer(p_array)
        
        return o_str
        #
        # End of XXX
        #


    # -----------------------------------------------------------------------
    #
    # fetchChangedSources
    #
    # -----------------------------------------------------------------------
    def fetchChangedSources(self, p_array):
        """
        Returns an array of source keys which have been changed since the
        given date. Use artist_key=0 to fetch all sources.
        Values passed in: service key, show key
        """
        
        p_array['method'] = 'fetchChangedSources'

        o_str = self.__requestAnswer(p_array)
        
        return o_str
        #
        # End of XXX
        #


    # -----------------------------------------------------------------------
    #
    # fetchChangedArtists
    #
    # -----------------------------------------------------------------------
    def fetchChangedArtists(self, p_array):
        """
        Returns an array of artist_keys which have been changed since the
        given date.
        Values passed in: service key, show key
        """

        p_array['method'] = 'fetchChangedArtists'

        o_str = self.__requestAnswer(p_array)
        
        return o_str
        #
        # End of XXX
        #


    # -----------------------------------------------------------------------
    #
    # fetchShowSources
    #
    # -----------------------------------------------------------------------
    def fetchShowSources(self, p_array):
        """
        Returns an array of source keys for the given show.
        Values passed in: service key, show key
        """

        p_array['method'] = 'fetchShowSources'

        o_str = self.__requestAnswer(p_array)
        
        return o_str
        #
        # End of XXX
        #


    # -----------------------------------------------------------------------
    #
    # fetchRecentSources
    #
    # -----------------------------------------------------------------------
    def fetchRecentSources(self, p_array):
        """
        Returns an array of source keys which have been entered since the
        given date. Use artist_key=0 to fetch all sources. This function
        returns a pure list of new seeds and does not contain sources which
        were edited during the query time period.
        Values passed in: service key, show key
        """

        p_array['method'] = 'fetchRecentSources'

        o_str = self.__requestAnswer(p_array)
        
        return o_str
        #
        # End of XXX
        #


    # -----------------------------------------------------------------------
    #
    # fetchShowTorrents
    #
    # -----------------------------------------------------------------------
    def fetchShowTorrents(self, p_array):
        """
        Return details for all torrents for the given show. 
        """
        
        p_array['method'] = 'fetchShowTorrents'

        o_str = self.__requestAnswer(p_array)
        
        return o_str
        #
        # End of fetchShowTorrents
        #


    # -----------------------------------------------------------------------
    #
    # fetchRecentTorrents
    #
    # -----------------------------------------------------------------------
    def fetchRecentTorrents(self, p_array):
        """
        Return details for all torrents since $year-$month-$day for the optional
        artist (use $artist_key = 0 for all) limited to the last $limit torrents.
        $limit is capped at 50.  
        """
        
        p_array['method'] = 'fetchRecentTorrents'

        o_str = self.__requestAnswer(p_array)
        
        return o_str
        #
        # End of fetchRecentTorrents
        #


    # -----------------------------------------------------------------------
    #
    # updateSource
    #
    # -----------------------------------------------------------------------
    def updateSource(self, p_array):
        """
        Updates the given source id with the field value. Valid fields
        are archive_identifier, textdoc,media_size, and comments. You
        must be a source admin to update your sources. This will also
        update the last edited user field and the last update date.
        Changes are audited but changes are not put through the peer
        review process.
        Values passed in: service key, show key
        """

        p_array['method'] = 'updateSource'

        o_str = self.__requestAnswer(p_array)
        
        return o_str
        #
        # End of XXX
        #


    # -----------------------------------------------------------------------
    #
    # createMd5
    #
    # -----------------------------------------------------------------------
    def createMd5(self, p_array):
        """
        Create a new md5 record for the given source key. Returns the new
        md5 key. You must be a source admin to add md5s to a source.
        Values passed in: service key, show key
        """

        p_array['method'] = 'createMd5'

        o_str = self.__requestAnswer(p_array)
        
        return o_str
        #
        # End of XXX
        #


    # -----------------------------------------------------------------------
    #
    # deleteMd5
    #
    # -----------------------------------------------------------------------
    def deleteMd5(self, p_array):
        """
        Delete an md5 record from a source. You must provide the md5 key
        which is available through the fetchSource method. You must be a
        source admin to delete md5s from a source.
        Values passed in: service key, show key
        """

        p_array['method'] = 'deleteMd5'

        o_str = self.__requestAnswer(p_array)
        
        return o_str
        #
        # End of XXX
        #


    # -----------------------------------------------------------------------
    #
    # returnArtistKey
    #
    # -----------------------------------------------------------------------
    def returnArtistKey(self, p_array):
        """
        Return the artist key. For a given artist name this method returns
        the artist key as a string.
        Values passed in: service key, show key
        """
        
        artist_key = 0
        
        r_str = self.lookupArtist(p_array)
        a_xml = minidom.parseString(r_str)
        
        elem = a_xml.getElementsByTagName('artist_key')[0]
        artist_key = self.__getText(elem.childNodes)

        return artist_key
        #
        # End of returnArtistKey
        #


    # -----------------------------------------------------------------------
    #
    # returnShnInfoKeyList
    #
    # -----------------------------------------------------------------------
    def returnShnInfoKeyList(self, p_array):
        """
        Return a list of SHN IDs for a given show
        """
        from lxml import etree

        shnid_list = []
         
        artist_key = self.returnArtistKey(p_array)
        p_array['artist_key']  = artist_key

        show_key = self.returnShowKey(p_array)
        p_array['shows_key'] = show_key
    
        source_xml = self.fetchShowSources(p_array)

        root = etree.XML(source_xml)

        Source = root[0][0]
        shnid_list = list([key.text for key in Source])

        if self.debug:
            print(shnid_list)
         
        return shnid_list
        #
        # End of returnShnInfoKeyList
        #


    # -----------------------------------------------------------------------
    #
    # returnShowKey
    #
    # -----------------------------------------------------------------------
    def returnShowKey(self, pa):
        """
        Return the artist key. For a given artist name this method returns
        the artist key as a string
        Values passed in: service key, artist key, year, month, day
        """
        
        artist_key = pa['artist_key']
        year       = pa['year']
        month      = pa['month']
        day        = pa['day']

        show = self.lookupShow(pa) 

        root = ET.fromstring(show)

        for child1 in root:
            for child2 in child1:
                for child3 in child2:
                    show_key = child3.find('shows_key').text
                    
        #a = root[0][0][0][2].text
                         
        return show_key
        #
        # End of returnShowKey
        #

        
    # -----------------------------------------------------------------------
    #
    # __getText
    #
    # -----------------------------------------------------------------------
    def __getText(self, nodelist):
        """
        This private method walks the nodelist returning the text portion
        of the XML node.
        """
        
        rc = []
        for node in nodelist:
            if node.nodeType == node.TEXT_NODE:
                rc.append(node.data)
                
        return ''.join(rc)
        #
        # End of __getText
        #


    #
    # End of class MB_EtreeRest
    #

# -----------------------------------------------------------------------
#
#                       < End of MB_EtreeRest.py >
#
# -----------------------------------------------------------------------
