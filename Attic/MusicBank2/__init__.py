# -----------------------------------------------------------------------
#
#                             < __init__.py >
#
# -----------------------------------------------------------------------


# -----------------------------------------------------------------------
#
# File Name    : __init__.py
#
# Author       : Josef Grosch
#
# Date         : 18 June 2015
#
# Modification : Some
#
# Application  :
#
# Description  :
#
# Notes        :
#
# Functions    :
#
# -----------------------------------------------------------------------


# -----------------------------------------------------------------------
#
#                              Copyright
#
#                   (C) Copyright 2015 Moose River LLC.
#                          < jgrosch@gmail.com >
#
#                         All Rights Reserved
#
#                 Deadicated to my brother Jerry Garcia,
#              who passed from this life on August 9, 1995.
#                       Happy trails to you, Jerry
#
# -----------------------------------------------------------------------


# -----------------------------------------------------------------------
#
#                               GPG Key
#
# pub   4096R/9FDD3FC7 2012-04-29
# Key   fingerprint = 178C 7996 9292 B705 7067  7084 9EA9 459F 9FDD 3FC7
# uid   Josef Grosch <jgrosch@gmail.com>
# sub   4096R/EEA1791E 2012-04-29
#
# -----------------------------------------------------------------------


# -----------------------------------------------------------------------
#
#                         Contact Information
#
#                          Moose River LLC.
#                            P.O. Box 9403
#                         Berkeley, Ca. 94709
#
#                      http://www.mooseriver.com
#
# -----------------------------------------------------------------------


# -----------------------------------------------------------------------
#
# Import
#
# -----------------------------------------------------------------------

"""
MusicBank

The main package for the MusicBank code base.

"""

#__revision__ = "$Id: __init__.py,v 1.2 2015/05/29 22:39:39 jgrosch Exp $"

#--start constants--

__author__     = "Josef Grosch"
__copyright__  = "Copyright 2015 Moose River, LLC."
__license__    = "Apache License v2.0"
__version__    = "0.1"
__maintainer__ = "Josef Grosch"
__email__      = "jgrosch@gmail.com"
__status__     = "Development"

#--end constants--

from MB_EtreeRest import MB_EtreeRest
from BandNames import BandNames

