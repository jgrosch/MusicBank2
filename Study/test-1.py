#!/usr/bin/python

import os, sys
import MooseRiver as MR
import MooseRiver.MR_util as MRU

# $Id: test-1.py,v 1.3 2015/10/19 22:37:23 jgrosch Exp $

def main():

    FirstTime = True
    len_count = 8
    
    #a = MR.getRandomString(FirstTime, len_count)
    a = MRU.genRandomString(FirstTime, len_count)
    
    sys.exit(1)

#
# Entry point
#
if __name__ == '__main__':
    main()
