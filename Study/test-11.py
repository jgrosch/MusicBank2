#!/usr/bin/env python

import sys, os, re

def getAmazonKeys():
    #key_file = '/usr/local/site/etc/vault/aws-keys.txt'
    AWS = {}
    my_home = os.environ['HOME']
    site_base = os.environ['SITE_HOME']

    key_file = "%s/aws-keys.txt" % (site_base)
    
    fp = open(key_file, 'r')
    lines = fp.readlines()
    fp.close()

    for line in lines:
        bits = line.split('=')
        key = bits[0].strip()
        value = bits[1].strip()
        AWS[key] = value

    print AWS
    return AWS

def main():

    D = getAmazonKeys()
    
    sys.exit(1)

if __name__ == '__main__':
    main()
    

    
