#!/usr/bin/python

import os, sys
#from amazon.api import AmazonAPI
import bottlenose
from pprint import pprint
from lxml import etree


# 93624893523  - Neail Young
# 724353937829 - One nation under groove

# $Id: test-7.py,v 1.1 2015/11/23 00:05:17 jgrosch Exp $

def getAmazonKeys():
    #key_file = '/usr/local/site/etc/vault/aws-keys.txt'
    AWS = {}
    my_home = os.environ['HOME']
    site_base = os.environ['SITE_HOME']

    key_file = "%s/aws-keys.txt" % (site_base)
    
    fp = open(key_file, 'r')
    lines = fp.readlines()
    fp.close()

    for line in lines:
        line = line.strip()
        line_len = len(line)
        if line_len <= 0:
            continue
        bits = line.split('=')
        key = bits[0].strip()
        value = bits[1].strip()
        value = value.strip('"')
        AWS[key] = value

    return AWS
    #
    # End of getAmazonKeys
    #

def main():

    D = getAmazonKeys()

    AWS_ACCESS_KEY_ID     = D['AMAZON_ACCESS_KEY']
    AWS_SECRET_ACCESS_KEY = D['AMAZON_SECRET_KEY']
    AWS_ASSOCIATE_TAG     = D['AMAZON_ASSOC_TAG']

    
    amazon = bottlenose.Amazon(AWS_ACCESS_KEY_ID, AWS_SECRET_ACCESS_KEY, AWS_ASSOCIATE_TAG)

    product = amazon.ItemLookup(IdType='UPC', ItemId='724353937829', SearchIndex='Music')
    root = etree.fromstring(product)
    print etree.tostring(root, pretty_print=True)

    #response = amazon.ItemLookup(ItemId="B007OZNUCE")025192110306
    product = amazon.ItemLookup(IdType='UPC', ItemId='025192110306', SearchIndex='Movies')
    root = etree.fromstring(product)
    print etree.tostring(root, pretty_print=True)
    
    sys.exit(1)

#
# Entry point
#
if __name__ == '__main__':
    main()


