#!/usr/bin/python

import os, sys
#from amazon.api import AmazonAPI
import bottlenose
from pprint import pprint
from lxml import etree
import MusicBank2.MusicBank_utils as MBU


# 93624893523  - Neil Young
# 724353937829 - One nation under groove


def main():

    D = MBU.getAmazonKeys()

    AWS_ACCESS_KEY_ID     = D['AMAZON_ACCESS_KEY']
    AWS_SECRET_ACCESS_KEY = D['AMAZON_SECRET_KEY']
    AWS_ASSOCIATE_TAG     = D['AMAZON_ASSOC_TAG']

    
    amazon = bottlenose.Amazon(AWS_ACCESS_KEY_ID, AWS_SECRET_ACCESS_KEY, AWS_ASSOCIATE_TAG)

    product = amazon.ItemLookup(IdType='UPC', ItemId='724353937829', SearchIndex='Music')
    root = etree.fromstring(product)
    print etree.tostring(root, pretty_print=True)

    #response = amazon.ItemLookup(ItemId="B007OZNUCE")025192110306
    product = amazon.ItemLookup(IdType='UPC', ItemId='025192110306', SearchIndex='Movies')
    root = etree.fromstring(product)
    print etree.tostring(root, pretty_print=True)
    
    sys.exit(1)

#
# Entry point
#
if __name__ == '__main__':
    main()


