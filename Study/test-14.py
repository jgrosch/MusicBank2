#!/usr/bin/python

import os, sys
#from amazon.api import AmazonAPI
import bottlenose
from pprint import pprint
from lxml import etree
import MusicBank2.MusicBank as MB
import MooseRiver2.MR_util as MRU


# 93624893523  - Neil Young
# 724353937829 - One nation under groove

def convertMusicbranzResultToHash(result):
    D = {}

    release_count = result['release-count']
    for index1 in range(release_count):
        ext_score = int(result['release-list'][index1]['ext:score'])
        if ext_score == 100:
            D['rec_version'] = 0
            D['user_ranking'] = 0
            D['reviewer_ranking'] = 0
            D['ext_score'] = ext_score
            
            D['asin'] = result['release-list'][index1]['asin']
            D['barcode'] = result['release-list'][index1]['barcode']
            D['country'] = result['release-list'][index1]['country']
            D['rel_date'] = result['release-list'][index1]['date']
            D['album_id_musicbranz'] = result['release-list'][index1]['id']
            
            D['medium_count'] = result['release-list'][index1]['medium-count']
            D['status'] = result['release-list'][index1]['status']
            D['album_title'] = result['release-list'][index1]['title']
            
            D['artist_name'] = result['release-list'][index1]['artist-credit'][0]['artist']['name']
            D['artist_id_musicbranz'] = result['release-list'][index1]['artist-credit'][0]['artist']['id']
            D['sort_name'] = result['release-list'][index1]['artist-credit'][0]['artist']['sort-name']

            D['catalog_number'] = result['release-list'][index1]['label-info-list'][0]['catalog-number']
            D['label_name'] = result['release-list'][index1]['label-info-list'][0]['label']['name']
            D['label_id_musicbranz'] = result['release-list'][index1]['label-info-list'][0]['label']['id']

            D['id_type'] = MRU.USER_ID
            D['album_id_musicbank'] = MRU.getIdByType(D)
            D['id_type'] = MRU.ARTIST_ID
            D['artist_id_musicbank'] = MRU.getIdByType(D)
            D['id_type'] = MRU.LABLE_ID
            D['label_id_musicbank'] =  MRU.getIdByType(D)

            media_list = result['release-list'][index1]['medium-list']
            ml_len = len(media_list)
            for index2 in range(len(media_list)):
                if 'format' in result['release-list'][index1]['medium-list'][index2]:
                    D['media_type'] = result['release-list'][index1]['medium-list'][index2]['format']
                else:
                    continue
        else:
            continue

    return D
    #
    # End of convertMusicbranzResultToHash
    #

def main():

    D = {}
    #D['barcode'] = '07464404882'   # Sticky Fingers ?
    #D['barcode'] = '077774600125'  # Dark Side Of The Moon?
    #D['barcode'] = '724353937829'  # One Nation Under A Grove
    D['barcode'] = '886972666228'  # At Budokan
    #D['barcode'] = '752211101723'  # Shake Some Action
    
    #D['catno] = 'ck40488'     # Sticky Fingers ?
    #D['catno] = 'cdp7460012'  # Dark Side Of The Moon?
    #D['catno] = 'ek86448'     # At Budokan
    #D['catno] = 'aim1017cd'   # Shake Some Action

    M = MB.MB_Base()
    
    D2 = M.askMusicBranz(D)
    result = D2['result']
    jdump  = D2['jdump']
    
    pprint(result)

    release_count = result['release-count']
    for index1 in range(release_count):
        
        ext_score = int(result['release-list'][index1]['ext:score'])
        if ext_score == 100:
            D['rec_version'] = 0
            D['user_ranking'] = 0
            D['reviewer_ranking'] = 0
            D['ext_score'] = ext_score
            D['asin'] = result['release-list'][index1]['asin']
            D['barcode'] = result['release-list'][index1]['barcode']
            D['country'] = result['release-list'][index1]['country']
            D['rel_date'] = result['release-list'][index1]['date']
            D['album_id_musicbranz'] = result['release-list'][index1]['id']
            
            D['medium_count'] = result['release-list'][index1]['medium-count']
            D['status'] = result['release-list'][index1]['status']
            D['album_title'] = result['release-list'][index1]['title']
            
            D['artist_name'] = result['release-list'][index1]['artist-credit'][0]['artist']['name']
            D['artist_id_musicbranz'] = result['release-list'][index1]['artist-credit'][0]['artist']['id']
            D['artist_id_musicbank'] = ''
            D['sort_name'] = result['release-list'][index1]['artist-credit'][0]['artist']['sort-name']

            D['catalog_number'] = result['release-list'][index1]['label-info-list'][0]['catalog-number']
            D['label_name'] = result['release-list'][index1]['label-info-list'][0]['label']['name']
            D['label_id_musicbranz'] = result['release-list'][index1]['label-info-list'][0]['label']['id']
            D['label_id_musicbank'] = ''

            media_list = result['release-list'][index1]['medium-list']
            ml_len = len(media_list)
            for index2 in range(len(media_list)):
                if 'format' in result['release-list'][index1]['medium-list'][index2]:
                    D['media_type'] = result['release-list'][index1]['medium-list'][index2]['format']
                else:
                    continue
        else:
            continue

    pprint(D)
    
    sys.exit(1)

#
# Entry point
#
if __name__ == '__main__':
    main()


