#!/usr/bin/python

import os, sys
import MooseRiver as MR
import MooseRiver.MR_util as MRU
import MooseRiver.MR_db_util as MRD

# $Id: test-2.py,v 1.3 2015/10/20 21:58:27 jgrosch Exp $

def main():

    FirstTime = True
    len_count = 8

    db_file = '/usr/local/etc/MooseRiver/redwood-MusicBank-db.xml'
    
    a = MRU.genRandomString(FirstTime, len_count)

    R = MRD.connectToDatabaseXML(db_file)
    dbh = R['dbh']
    mdb = R['mdb']
    
    sys.exit(1)

#
# Entry point
#
if __name__ == '__main__':
    main()
