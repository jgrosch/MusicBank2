#!/usr/bin/python

import os, sys
import MooseRiver as MR
import MooseRiver.MR_util as MRU
import MooseRiver.MR_db_util as MRD
import MooseRiver.MR_MBSearch as MRS


# $Id: test-3.py,v 1.3 2015/11/01 20:00:15 jgrosch Exp $

def main():

    # Nina Hartley's Guid to Anal Sex - 799613063297
    # Tremors                         - 25192021824
    # The Day The Earth Stood Still   - 24543050056

    FirstTime = True
    len_count = 8

    db_file = '/usr/local/etc/MooseRiver/redwood-MusicBank-db.xml'
    
    a = MRU.genRandomString(FirstTime, len_count)

    R = MRD.connectToDatabaseXML(db_file)
    dbh = R['dbh']
    mdb = R['mdb']

    print 'Should return The Day Earth Stood Still.'
    R['barcode'] = '24543050056'
    xml_str = MRS.findDvdByBarcode(R)
    print xml_str

    print "Should return Nina Hartley's Guid to Anal Sex."
    R['barcode'] = '799613063297'
    xml_str = MRS.findDvdByBarcode(R)
    print xml_str

    print "Should return Tremors."
    R['barcode'] = '25192021824'
    xml_str = MRS.findDvdByBarcode(R)
    print xml_str

    sys.exit(1)

#
# Entry point
#
if __name__ == '__main__':
    main()
