#!/usr/bin/python

import os, sys
import musicbrainzngs
import json
from pprint import pprint

#import MooseRiver as MR
#import MooseRiver.MR_util as MRU
#import MooseRiver.MR_db_util as MRD
#import MooseRiver.MR_MBSearch as MRS

# 93624893523  - Neail Young
# 724353937829 - One nation under groove

# $Id: test-4.py,v 1.2 2015/11/06 16:28:02 jgrosch Exp $

def main():
    user_name = 'jgrosch'   # MUSICBRANZ_USER
    password  = 'TrustNo1!' # MUSICBRANZ_PASSWD

    musicbrainzngs.auth(user_name, password)
    musicbrainzngs.set_format(fmt='xml')
    musicbrainzngs.set_useragent(
        "MusicBank",
        "0.1",
        "http://musicbank.com/Pages/About/contact.php",
        )

    artist_id = "c5c2ea1c-4bde-4f4d-bd0b-47b200bf99d6"
    try:
        result = musicbrainzngs.get_artist_by_id(artist_id)
    except WebServiceError as exc:
        print("Something went wrong with the request: %s" % exc)
    else:
        artist = result["artist"]
        print("name:\t\t%s" % artist["name"])
        print("sort name:\t%s" % artist["sort-name"])


    a_barcode = '93624893523'
    result = musicbrainzngs.search_releases(barcode = a_barcode, limit=5)
    print result
    jdump = json.dumps(result, indent=2, sort_keys=True)
    print jdump

    a_barcode = '724353937829'
    result = musicbrainzngs.search_releases(barcode = a_barcode, limit=5)
    print result

    #with open(a_barcode) as data_file:    
    #    data = json.load(data_file)
    #    pprint(data)

    jdump = json.dumps(result, indent=2, sort_keys=True)
    print jdump
    asin = result['release-list'][0]['asin']
        
    sys.exit(1)

#
# Entry point
#
if __name__ == '__main__':
    main()


