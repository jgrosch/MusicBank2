#!/usr/bin/python

import os, sys
import musicbrainzngs
import json
from pprint import pprint

#import MooseRiver as MR
#import MooseRiver.MR_util as MRU
#import MooseRiver.MR_db_util as MRD
#import MooseRiver.MR_MBSearch as MRS

# 93624893523  - Neail Young
# 724353937829 - One nation under groove

# $Id: test-5.py,v 1.4 2015/11/24 19:45:14 jgrosch Exp $

def main():
    user_name = 'jgrosch'
    password  = 'TrustNo1!'

    musicbrainzngs.auth(user_name, password)
    #musicbrainzngs.set_format(fmt='xml')
    musicbrainzngs.set_useragent(
        "MusicBank",
        "0.1",
        "http://musicbank.com/Pages/About/contact.php",
        )


    a_barcode = '724353937829'
    result = musicbrainzngs.search_releases(barcode = a_barcode, limit=5)
    
    print result
    #with open(a_barcode) as data_file:    
    #    data = json.load(data_file)
    #    pprint(data)
    jdump = json.dumps(result, indent=2, sort_keys=True)
    print jdump
    
    release_count = result['release-count']
    if release_count == 0:
        print "CD not found."
        sys.exit(0)
    elif release_count > 1:
        print "More than 1 found."
        sys.exit(0)
    else:
        asin           = result['release-list'][0]['asin']
        title          = result['release-list'][0]['title']
        mb_release_id  = result['release-list'][0]['id']
        label          = result['release-list'][0]['label-info-list'][0]['label']['name']
        artist         = result['release-list'][0]['artist-credit'][0]['artist']['name']
        mb_artist_id   = result['release-list'][0]['artist-credit'][0]['artist']['id']
        barcode        = result['release-list'][0]['barcode']

    print "ASIN:          %s" % (asin)
    print "Title:         %s" % (title)
    print "MB Release ID: %s" % (mb_release_id)
    print "Label:         %s" % (label)
    print "Artist:        %s" % (artist)
    print "MB Artist ID:  %s" % (mb_artist_id)
    print "Barcode:       %s" % (barcode)
    
    sys.exit(1)

#
# Entry point
#
if __name__ == '__main__':
    main()


