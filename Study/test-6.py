#!/usr/bin/python

import os, sys
#from amazon.api import AmazonAPI
import bottlenose
from pprint import pprint
from lxml import etree


# 93624893523  - Neail Young
# 724353937829 - One nation under groove

# $Id: test-6.py,v 1.5 2015/11/23 00:05:17 jgrosch Exp $

def main():
    
    AWS_ACCESS_KEY_ID     = AMAZON_ACCESS_KEY
    AWS_SECRET_ACCESS_KEY = AMAZON_SECRET_KEY
    AWS_ASSOCIATE_TAG     = AMAZON_ASSOC_TAG

    #amazon = AmazonAPI(AMAZON_ACCESS_KEY, AMAZON_SECRET_KEY, AMAZON_ASSOC_TAG)
    amazon = bottlenose.Amazon(AWS_ACCESS_KEY_ID, AWS_SECRET_ACCESS_KEY, AWS_ASSOCIATE_TAG)

    response = amazon.ItemLookup(ItemId="B007OZNUCE")
    #response = amazon.ItemSearch(Keywords="Kindle 3G", SearchIndex="All")
    #response = amazon.ItemLookup(ItemId="1449372422", ResponseGroup="Images")
    #response = amazon.SimilarityLookup(ItemId="B007OZNUCE")

    product = amazon.ItemLookup(IdType='UPC', ItemId='724353937829', SearchIndex='Music')

    #xml_out = xml.dom.minidom.parse(product) 
    #pretty_xml_as_string = xml_out.toprettyxml()
    
    root = etree.fromstring(product)
    print etree.tostring(root, pretty_print=True)

    pprint(pretty_xml_as_string)
    
    
    title = product.title
    #a = product.price_and_currency
    #price = a[0]
    #currency = a[1]
    
    #b = product.ean
    #c = product.large_image_url
    #d = product.get_attribute('Publisher')
    #e = product.get_attributes(['ItemDimensions.Width', 'ItemDimensions.Height'])

    
    sys.exit(1)

#
# Entry point
#
if __name__ == '__main__':
    main()


