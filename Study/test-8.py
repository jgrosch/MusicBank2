#!/usr/bin/python

import os, sys
from pprint import pprint
from lxml import etree
import MusicBank2.MusicBank_user as MB2u
import crypt



#| insert_time   | varchar(32) | NO   |     | NULL    |                |
#| rec_id        | varchar(32) | NO   |     | NULL    |                |
#| user_id       | varchar(32) | YES  |     | NULL    |                |
#| email         | varchar(64) | NO   |     | NULL    |                |
#| active        | tinyint(1)  | NO   |     | NULL    |                |
#| status        | int(11)     | NO   |     | NULL    |                |
#| verified      | tinyint(1)  | NO   |     | NULL    |                |
#| active_cookie | varchar(64) | YES  |     | NULL    |                |
#| cookie_time   | varchar(32) | YES  |     | NULL    |                |
#| cookie_ttl    | varchar(32) | YES  |     | NULL    |                |
#| birthdate     | varchar(16) | YES  |     | NULL    |                |
#| gender        | int(11)     | YES  |     | NULL    |                |
#| zip           | varchar(8)  | NO   |     | NULL    |                |
#| city          | varchar(25) | NO   |     | NULL    |                |
#| county        | varchar(32) | YES  |     | NULL    |                |
#| state         | varchar(4)  | NO   |     | NULL    |                |
#| country       | varchar(4)  | NO   |     | NULL    |                |
#| last_update   | varchar(32) | YES  |     | NULL    |                |
#| first_name    | varchar(32) | YES  |     | NULL    |                |
#| last_name     | varchar(32) | YES  |     | NULL    |                |
#| display_name  | varchar(32) | YES  |     | NULL    |                |
#| password      | varchar(32) | NO   |     | NULL    |                |
#| subscribe     | tinyint(1)  | NO   |     | NULL    |                |
#| phone         | varchar(16) | NO   |     | NULL    |                |

def main():

    a = MB2u.User()
    b = a.getTableName()

    a.setRecId('1q2w3e4r5t6y')
    a.setInsertTime('1448571911')
    a.setActive(True)
    a.setActiveCookie('1q2w3e4r5t')
    a.setBirthdate('1 May 1967')
    a.setCity('Berkeley')
    a.setCookieTime('1448571911')
    a.setCookieTtl(86400)
    a.setCountry('US')
    a.setCounty('Cook')
    a.setDisplayName('Josef')
    a.setEmail('jgrosch@gmail.com')
    a.setFirstName('Josef')
    a.setGender(1)
    a.setLastName('Grosch')
    a.setLastUpdateTime('1448571911')

    pword = 'open2me'
    #salt = '1q2w3e4r5t6y7u8i9o0p'
    #cword = crypt.crypt(pword, salt)
    a.setPassword(pword)
    
    a.setPhone('510-207-9976')
    a.setState('CA')
    a.setStatus('Testing')
    a.setSubscribe(False)
    a.setUserId('ME')
    a.setVerified(True)
    a.setZipCode('94709')

    out_str = a.dumpRecord()
    print out_str

    query = a.toInsertSQL()
    print query
    
    sys.exit(1)

#
# Entry point
#
if __name__ == '__main__':
    main()


