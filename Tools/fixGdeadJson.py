#!/usr/bin/env python3

import os, sys
import json
import pprint
import argparse

def main():
    parser = argparse.ArgumentParser()

    parser.add_argument("--year", help='Year to process', required=True)

    args = parser.parse_args()

    baseDir = '/usr3/home/jgrosch/Git/MusicBank/Data/Grateful-Dead/json'
    inFile = f"{baseDir}/Raw/{args.year}.json"
    outFile = f"{baseDir}/Cooked/{args.year}.FIX.json"
    newDict = {}
    
    with open(inFile, 'r') as fh:
        gdDict = json.load(fh)

    Shows = gdDict['db_rest']['fetchArtistYear']['shows']
    showCount = len(Shows)

    for key in Shows:
        show = Shows[key]
        set1 = show['set1']
        set2 = show['set2']
        set3 = show['set3']
        showKey = show['shows_key']

        #print(f"Show {showKey} - Key {key}")
        doPrint = False
        
        # Set 1
        if len(set1) > 0:
            if isinstance(set1, str):
                setList = splitString(set1)
                show['set1'] = setList
                doPrint = True
                            
        # Set 2
        if len(set2) > 0:
            if isinstance(set2, str):
                setList = splitString(set1)                
                show['set2'] = setList
                doPrint = True
            
        # Set 3
        if len(set3) > 0:
            if isinstance(set3, str):
                setList = splitString(set1)
                show['set3'] = setList
                doPrint = True

        newDict[key] = show
        
        #if doPrint:
        #    pprint.pprint(show)

    gdDict['db_rest']['fetchArtistYear']['shows'] = newDict
    outStr = json.dumps(gdDict, indent=2)
    with open(outFile, 'w') as fh:
        fh.write(outStr)
        
    sys.exit(0)
    # End of main

def splitString(inStr):
    newList = []
    bits = inStr.split(',')
    for entry in bits:
        newList.append(entry.strip())
        
    return newList

if __name__ == '__main__':
    main()
