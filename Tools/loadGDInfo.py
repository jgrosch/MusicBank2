#!/usr/bin/env python3

import os
import sys
import json
import sqlite3
import uuid
import argparse

def main():
    D       = {}
    index   = 0
    gitHome = os.getenv('GIT_HOME2')
    base    = f"{gitHome}/MusicBank2/Data/Grateful-Dead"
    dbFile  = f"{base}/GD-Shows.sqlite"
    outFile = ''

    parser = argparse.ArgumentParser()

    parser.add_argument("--year", help='Year to process', required=True)

    parser.add_argument("--output", help='write out insert statements',
                        action='store_true')

    parser.add_argument("--quiet", help='No stdout output', action='store_true')

    parser.add_argument("--insert", help='Insert query', action='store_true')

    args = parser.parse_args()

    year = args.year
    yearInt = int(year)
    
    if yearInt < 1965 or yearInt > 1995:
        print("Error: year must be between 1965 and 1995")
        sys.exit(1)
        
    inFile = f"{base}/json/Raw/{year}.json"

    if args.output:
        outFile = f"{base}/Study/{year}.txt"
        outFP = open(outFile, 'w')

    if not args.quiet:
        print(f"\nYear: {year}\n")

    if args.output:
        outFP.write(f"\nYear: {year}\n")
    
    with open(inFile, 'r') as fp:
        jData = json.load(fp)

    Shows = jData['db_rest']['fetchArtistYear']['shows']
    showCount = len(Shows)
    
    insertCount = 0
    conn = sqlite3.connect(dbFile)
    with conn:
        cur = conn.cursor()

        for x in range(showCount):
            D['rec_id'] = "GDS-{}".format(uuid.uuid4())
            index = f"key_{x}"
            show = Shows[index]
            D['name'] = show['name']
            D['artist_key'] = show['artist_key']
            D['shows_key'] = show['shows_key']
            
            D['showdate'] = show['showdate']
            bits = D['showdate'].split('/')
            D['show_month'] = bits[0]
            D['show_day'] = bits[1]
            
            D['venue'] = cleanString(show['venue'])
            D['city'] = cleanString(show['city'])
            D['state'] = show['state']
            D['set1'] = cleanString(show['set1'])
            D['set2'] = cleanString(show['set2'])
            D['set3'] = cleanString(show['set3'])
            D['comment'] = cleanString(show['comment'])
            D['lastupdate'] = show['lastupdate']
            D['showyear'] = show['showyear']
            D['showsuserid'] = show['showsuserid']

            query = ("insert into setlists ( "
                     " rec_id,  band, artist_key, show_key, "
                     " show_date, venue, city, state_abbr, "
                     " set_1, set_2, set_3, comment, "
                     " last_update, show_year, show_user_id, "
                     " show_month, show_day) "
                     " values ( \"{}\", \"{}\", \"{}\", \"{}\", "
                     " \"{}\", \"{}\", \"{}\", \"{}\", "
                     " \"{}\", \"{}\", \"{}\", \"{}\", "
                     " \"{}\", \"{}\", \"{}\", \"{}\", \"{}\") ;"
                     .format(D['rec_id'], D['name'], D['artist_key'], 
                             D['shows_key'], D['showdate'], D['venue'], 
                             D['city'], D['state'], D['set1'], D['set2'], 
                             D['set3'], D['comment'], D['lastupdate'], 
                             D['showyear'], D['showsuserid'],
                             D['show_month'], D['show_day']))

            if not args.quiet:
                print(f"\n#\n# Key: {index}\n# -----------------------------------\n")
                print(query)

            if args.output:
                outFP.write(f"\n#\n# Key: {index}\n# -----------------------------------\n")
                outFP.write(query)

            if args.insert:
                print(f"Key: {index}")
                cur.execute(query)
                insertCount += 1

            if x == (showCount - 1):
                i = 0
            # End of for loop
        # End of with

    if args.output:
        outFP.close()

    conn.commit()
    cur.close()
    conn.commit()
    
    sys.exit(0)
    # End of main

# --------------------------------------------------------------------
#
# cleanString
#
# --------------------------------------------------------------------
def cleanString(inStr):
    outStr = ""
    strChanged = False
    
    if '"' in inStr:
        outStr = inStr.replace('"', '')
        strChanged = True
    else:
        outStr = inStr

    if strChanged == False:
        if "'" in inStr:
            outStr = inStr.replace("'", '')
            strChanged = True
        else:
            outStr = inStr
        
    return outStr
    #
    # End of cleanString
    #

if __name__ == '__main__':
    main()
